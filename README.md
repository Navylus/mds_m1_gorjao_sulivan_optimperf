## Projet de GORJAO Sulivan

# Deployer l'application avec Docker

Il est nécessaire d'avoir docker et docker compose d'installé sur la machine pour pouvoir faire tourner les différents clusters.

Il suffis de ce rendre dans ce dossier courant et d'executer la comment _docker-compose up --build_ L'application tourne ensuite sur _http://localhost:3000_

ATTENTION: la mise e place du service peut prendre 2 minutes environ.
# Récupération de l'addresse de connection

### Sur votre compte compte atlas :

- Dans votre nouveau cluster cliquer sur **connect**
- cliquer sur **Add Your Current IP Address** puis **Add IP address**
- créer l'utilisateur avec les crédentials de votre choix
- dans le panneau suivant choisir **Connect Your Application** puis copier le lien de connection
- c'est finis, félicitation!!

# Mettre en place un cluster local
- installer Docker suivant votre OS
- dans un terminal
- __docker pull mongo__
- __docker network create my-mongo-cluster__
~~~~
docker run \
-p 30001:27017 \
--name mongo1 \
--net my-mongo-cluster \
mongo mongod --replSet my-mongo-set

docker run \
-p 30002:27017 \
--name mongo2 \
--net my-mongo-cluster \
mongo mongod --replSet my-mongo-set

docker run \
-p 30003:27017 \
--name mongo3 \
--net my-mongo-cluster \
mongo mongod --replSet my-mongo-set
~~~~
- vous devez vous connecter à une instance pour vérifier que tout marche bien et mettre en place la config de réplication: 

- __docker exec -it mongo1 mongo__

~~~~~
> db = (new Mongo('localhost:27017')).getDB('project')
test
> config = {
  	"_id" : "my-mongo-set",
  	"members" : [
  		{
  			"_id" : 0,
  			"host" : "mongo1:27017"
  		},
  		{
  			"_id" : 1,
  			"host" : "mongo2:27017"
  		},
  		{
  			"_id" : 2,
  			"host" : "mongo3:27017"
  		}
  	]
  }
~~~~~
- La variable de connection est la suivant : __mongodb://localhost:27017/project__

# Lancement de l'application

- cloner ce project **git clone http://gitlab.com/navylus/projet_mongo**
- **cd projet_mongo**
- **npm i**
- créer un fichier .env rempli avec le lien de connection récupéré plus tot sous le format suivant : **export CONNECTION_URI=votre_lien_de_connection \n export CONNECTION_URI_LOCAL=votre_lien_de_connection_local**
- LES DEUX VARIABLES DOIVENT ETRES DEFINIES
- **source .env**
- **npm run start** pour se connecter au cluster Atlas
- **npm run local** pour se connecter au cluster Local
- le projet tourne sur: **http://localhost:3000**

# Informations

- nom du projet : Project0
- Nom du cluster : cluster-project

# routes de l'API:

**GET http://localhost:3000/tasks**

- Lister toutes les taches dans la base de données

**POST http://localhost:3000/tasks**

- Insère la tache dans la base de donnée.
  - taskName : Nom de la tache (REQUIS)
  - createdOn : Date de création de la tache(optionnel)

**GET http://localhost:3000/tasks:taskId**

- Accède à la tache dont l'id à été donné dans l'url

**PUT http://localhost:3000/tasks:taskId**

- Update la tache dont l'id à été donné dans l'url
  - taskName : Nom de la tache (REQUIS)
  - createdOn : Date de création de la tache(optionnel)

**DELETE http://localhost:3000/tasks:taskId**

- Supprime la tache dont l'id à été donné dans l'url


**LE PORT PEUT ÊTRE OVERRIDE AVEC LA VARIABLE D'ENVIRONEMENT "PORT"**
# Diagramme d'architecture
![Diagramme d'architecture](./diagram.png)
