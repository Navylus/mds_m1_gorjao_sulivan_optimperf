const express = require('express')
const bodyParser = require('body-parser')
const taskController = require('./controllers/TaskController')
const database = require('./config/dataBase.js')

// create express app
const app = express()

// Make the app run on port 3000 or on port explicitly set with an ENV Variable
const port = process.env.PORT || 3000

// Setup body parser to get url informations
app.use(bodyParser.urlencoded({ extended: true }))
app.use(bodyParser.json())

app.get('/', function(req, res) {
  database.initializeMongo()

  res.send('Hello Mongo!!!')
})

// our API routes, it allows us to do CURD operations on a simple Task API
app
  .route('/tasks')
  .get(taskController.listAllTasks)
  .post(taskController.createNewTask)

app
  .route('/tasks/:taskid')
  .get(taskController.readTask)
  .put(taskController.updateTask)
  .delete(taskController.deleteTask)

app.listen(port, () => {
  console.log(`Server running at http://localhost:${port}`)
})
