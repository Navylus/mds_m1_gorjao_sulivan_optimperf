const mongoose = require('mongoose')

// Connect to our Cluster. All informations come from dbURI which is collected in ENV variables
exports.initializeMongo = function() {
  console.log('Disconnecting from db just in case!')
  mongoose.disconnect()

  mongoose.connect('mongodb://mongo-rs01,mongo-rs02,mongo-rs03/project?replicaSet=rs0')

  var db = mongoose.connection
  db.on(
    'error',
    console.error.bind(
      console,
      'connection error: We might not be as connected as I thought'
    )
  )
  db.once('open', function() {
    console.log('We are connected you and I!')
  })
}

// Import Task model created in Task.js
require('../models/Task')
