// Import task model in order to create CURD operations
const Task = require('../models/Task')

// List all tasks and throw error 500 if not connected or if an error occure
exports.listAllTasks = (req, res) => {
  Task.find({}, (err, task) => {
    if (err) {
      res.status(500).send(err)
    }
    res.status(200).json(task)
  })
}


// Create a new task with informations send via POST request
exports.createNewTask = (req, res) => {
  const newTask = new Task(req.body)
  console.log(newTask)
  newTask.save((err, task) => {
    if (err) {
      res.status(500).send(err)
    }
    res.status(201).json(task)
  })
}

// Read a specific Task with an ID procurred by form in request
exports.readTask = (req, body) => {
  Task.findById(req.params.taskid, (err, task) => {
    if (err) {
      res.status(500).send(err)
    }
    res.status(200).json(task)
  })
}

// Update a task via new informations and an ID given in request
exports.updateTask = (req, res) => {
  Task.findOneAndUpdate(
    { _id: req.params.taskid },
    req.body,
    { new: true },
    (err, task) => {
      if (err) {
        res.status(500).send(err)
      }
      res.status(200).json(task)
    }
  )
}

// Delete a task via an ID given in request
exports.deleteTask = (req, res) => {
  Task.remove({ _id: req.params.taskid }, (err, task) => {
    if (err) {
      res.status(404).send(err)
    }
    res.status(200).json({ message: 'Task successfully deleted' })
  })
}
