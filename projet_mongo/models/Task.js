const mongoose = require('mongoose')
const Schema = mongoose.Schema

// Create a mongoose schema named Task with a name and a created date
const TaskSchema = new Schema({
  taskName: {
    type: String,
    required: true
  },
  createdOn: {
    type: Date,
    default: Date.now
  }
})

// Export the model in order to access it on app.js
module.exports = mongoose.model('Tasks', TaskSchema)
